package io.virandry.camsand.adapter;

import io.virandry.camsand.etc.ProcessConstants;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
@ConfigurationProperties
public class SendEmailAdapter implements JavaDelegate {

  private static final Logger LOGGER = Logger.getLogger(SendEmailAdapter.class.getName());

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    String emailAddress = (String) execution.getVariable(ProcessConstants.VAR_NAME_EMAIL_ADDRESS);
    String emailContent = (String) execution.getVariable("emailContent");
    LOGGER.info("Sending Email to: " + emailAddress + "with content " + emailContent);
  }


}
