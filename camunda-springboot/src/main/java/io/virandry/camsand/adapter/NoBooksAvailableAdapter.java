package io.virandry.camsand.adapter;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
public class NoBooksAvailableAdapter implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String emailContent = "`Hi, your requested books are not available. Thanks.`";
        execution.setVariable("emailContent", emailContent);
    }
}
