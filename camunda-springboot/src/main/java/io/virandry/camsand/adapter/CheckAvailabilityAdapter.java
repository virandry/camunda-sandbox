package io.virandry.camsand.adapter;

import io.virandry.camsand.etc.ProcessConstants;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@ConfigurationProperties
public class CheckAvailabilityAdapter implements JavaDelegate {

  // dummy
  private List<String> availableIsbns;

  private CheckAvailabilityAdapter() {
    availableIsbns = new ArrayList<>();
    availableIsbns.add("112233");
    availableIsbns.add("667788");
  }

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    List<String> isbns = (ArrayList<String>) execution.getVariable(ProcessConstants.VAR_NAME_ISBNS);
    List<String> available = new ArrayList<>();
    List<String> unavailable = new ArrayList<>();
    String availability = ProcessConstants.VAR_VALUE_AVAILABILITY_FULL;

    for (String isbn : isbns) {
      boolean matched = Arrays.stream(this.availableIsbns.toArray()).anyMatch(isbn::equals);
      if(matched) available.add(isbn);
      else unavailable.add(isbn);
    }

    if (!available.isEmpty() && !unavailable.isEmpty()) availability = ProcessConstants.VAR_VALUE_AVAILABILITY_SEMI;
    else if (available.isEmpty() && !unavailable.isEmpty()) availability = ProcessConstants.VAR_VALUE_AVAILABILITY_NONE;

    execution.setVariable("available", available);
    execution.setVariable("unavailable", unavailable);
    execution.setVariable(ProcessConstants.VAR_NAME_AVAILABILITY, availability);
  }

}
