package io.virandry.camsand.adapter;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties
public class SomeBooksAvailableAdapter implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        List<String> list = (List<String>) execution.getVariable("unavailable");
        String emailContent = "`Hi, some books are not available: " + list.toString() + ". Thanks.`";
        execution.setVariable("emailContent", emailContent);
    }
}
