package io.virandry.camsand.adapter;

import io.virandry.camsand.etc.ProcessConstants;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
@ConfigurationProperties
public class LogAdapter implements JavaDelegate {
  private static final Logger LOGGER = Logger.getLogger(LogAdapter.class.getName());
  @Override
  public void execute(DelegateExecution execution) throws Exception {

    LOGGER.info("Availability: " + execution.getVariable(ProcessConstants.VAR_NAME_AVAILABILITY));
    List<String> list = (ArrayList<String>) execution.getVariable("available");
    LOGGER.info("No. of available" + list.size());
    LOGGER.info(list.get(0));
    LOGGER.info("Done");
  }


}
