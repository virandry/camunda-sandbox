package io.virandry.camsand.dto;

import java.util.List;

public class RentalFormDTO {
    private List<String> isbns;
    private String emailAddress;

    public RentalFormDTO() {
    }

    public RentalFormDTO(List<String> isbns, String emailAddress) {
        this.isbns = isbns;
        this.emailAddress = emailAddress;
    }

    public List<String> getIsbns() {
        return isbns;
    }

    public void setIsbns(List<String> isbns) {
        this.isbns = isbns;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "RentalFormDTO{" +
                "isbns=" + isbns +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
