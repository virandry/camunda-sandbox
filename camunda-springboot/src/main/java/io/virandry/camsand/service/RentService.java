package io.virandry.camsand.service;

import io.virandry.camsand.etc.ProcessConstants;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.variable.Variables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RentService {

    @Autowired
    private ProcessEngine camunda;

    public ProcessInstance rentBook(final List<String> isbns, final String emailAddress) {
        return camunda.getRuntimeService().startProcessInstanceByKey(
                ProcessConstants.PROCESS_KEY_RENT,
                Variables
                        .putValue(ProcessConstants.VAR_NAME_ISBNS, isbns)
                        .putValue(ProcessConstants.VAR_NAME_EMAIL_ADDRESS, emailAddress));
    }
}
