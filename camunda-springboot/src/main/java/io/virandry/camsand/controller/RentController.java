package io.virandry.camsand.controller;

import io.virandry.camsand.dto.RentalFormDTO;
import io.virandry.camsand.service.RentService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

@RestController
@RequestMapping("/rent")
public class RentController {

    private static final Logger LOGGER = Logger.getLogger(RentController.class.getName());

    @Autowired
    RentService rentService;

    @PostMapping
    public ResponseEntity rentBook(@RequestBody RentalFormDTO rentalFormDTO) {
        ProcessInstance instance = rentService.rentBook(rentalFormDTO.getIsbns(), rentalFormDTO.getEmailAddress());
        LOGGER.log(Level.INFO, "instanceId: " + instance.getId());
        return ResponseEntity.accepted().build();
    }
}
