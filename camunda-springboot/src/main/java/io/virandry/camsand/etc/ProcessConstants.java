package io.virandry.camsand.etc;

public class ProcessConstants {

  public static final String PROCESS_KEY_RENT = "rent";
  
  public static final String VAR_NAME_ISBNS = "isbns";
  public static final String VAR_NAME_EMAIL_ADDRESS = "emailAddress";
  public static final String VAR_NAME_RENT_ID = "rentId";
  public static final String VAR_NAME_AMOUNT = "amount";
  public static final String VAR_NAME_SHIPMENT_ID = "shipmentId";
/*  public static final String VARIABLE_paymentTransactionId = "paymentTransactionId";
  
  public static final String MSG_NAME_GoodsShipped = "Message_GoodsShipped";*/

  public static final String VAR_NAME_AVAILABILITY = "availability";
  public static final String VAR_VALUE_AVAILABILITY_FULL = "full";
  public static final String VAR_VALUE_AVAILABILITY_SEMI = "semi";
  public static final String VAR_VALUE_AVAILABILITY_NONE = "none";

}
